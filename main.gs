function buildPieChart()
{
  
  //var SS = SpreadsheetApp.openById(SpreadsheetApp.getActiveSpreadsheet().getId());
  //var SS = SpreadsheetApp.openById("1G27qyjxxVbPJ5mLGvmPFspjlXi08clKHZSzUsTyMaJ4");
  //var sheet = SS.getSheets()[0];

  /*To access spreadsheet by using Spreadsheet id.*/
  var SS = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = SS.getActiveSheet();
  
  var selection = sheet.getSelection();

  // Get data range
  //var data = sheet.getRange("K5:K18").getValues();
  var data = selection.getActiveRange().getValues();
  
  // Error Handling
  if (data[0][0] == ""){
    //ui.prompt("Please select the correct Range and re-run the script");
    throw new Error("Empty range selected. No data to process.")
  }
  if (data[0][0].split(" ").length > 3) {
    throw new Error("Please select only Task Status column.");
  }
 
  const data_dict = {};
  const outSliceColor = {};

  data.forEach(
    function(x){
      if (x[0] == '')
        return;
      data_dict[x[0]] = (data_dict[x[0]] || 0) + 1; 
    }
  );

  var pie_dataTable = Charts.newDataTable()
                              .addColumn(Charts.ColumnType.STRING, "Task Status")
                              .addColumn(Charts.ColumnType.NUMBER, "Value");

  var index = 0;
  for (const [key, value] of Object.entries(data_dict)){
    var setcolor;
    switch(key) {
      case "Awaiting dependency":
        setcolor = "yellow";
        break;

      case "Not Started":
        setcolor = "gainsboro";
        break;
      
      case "Completed":
        setcolor = "green";
        break;
      
      case "N/A":
        setcolor = "pink";
        break;

      case "Discarded":
        setcolor = "red";
        break;

      case "In progress":
        setcolor = "orange";
        break;      
    }

    outSliceColor[index] = {color: setcolor} 

    index += 1;
    pie_dataTable.addRow([key, value])
  }

  var name_of_interest = sheet.getRange(selection.getActiveRange().getRow(), 2).getValue();

  var chart = Charts.newPieChart()
                      .setDataTable(pie_dataTable)
                      .setTitle(name_of_interest)
                      .set3D()
                      .setOption('titleTextStyle', {fontSize: 20, color: "gray"})
                      .setOption('backgroundColor.strokeWidth', 1)
                      .setOption('slices', outSliceColor)
                      .setOption('chartArea', {left:40,top:80,width:"100%",height:"100%"})
                      .setOption('pieSliceTextStyle', {color: 'black'})
                      .setOption('pieSliceBorderColor', 'black')
                      .setOption('width', 400)
                      .setOption('height', 380)
                      .build();
  
  var htmlOutput = HtmlService.createHtmlOutput().setTitle('Pie Chart Automation');
  var imageData = Utilities.base64Encode(chart.getAs('image/png').getBytes());
  var imageUrl = "data:image/png;base64," + encodeURI(imageData);
  htmlOutput.append("Render chart server side: <br/>");
  htmlOutput.append("<img border=\"1\" src=\"" + imageUrl + "\">");

  var row = Math.floor((startRow_num + endRow_num)/2);
  var column = selection.getActiveRange().getColumn();
  var columnRange = selection.getActiveRange().getA1Notation();
  
  var startRow_num = parseInt(columnRange.split(":")[0].slice(1));
  var endRow_num = parseInt(columnRange.split(":")[1].slice(1));

  sheet.insertImage(imageUrl, column+2, row-1);

  //console.log(data_dict)
 // return htmlOutput
}

  // var manual_dataTable = Charts.newDataTable()
  //                             .addColumn(Charts.ColumnType.STRING, "Task Status")
  //                             .addColumn(Charts.ColumnType.NUMBER, "Value")
  //                             .addRow(["Not Started",1])
  //                             .addRow(["In Progress",2])
  //                             .addRow(["Completed", 2])
  //                             .addRow(["Discarded", 2])
  //                             .addRow(["Hold", 2])
  //                             .addRow(["N/A", 2])
  //                             .build();
